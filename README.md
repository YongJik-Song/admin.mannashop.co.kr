# admin.mannashop.co.kr

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## 가이드 링크
[**공통 Vuex 컴포넌트 가이드 바로가기**](https://bitbucket.org/YongJik-Song/admin.mannashop.co.kr/wiki/%EA%B3%B5%ED%86%B5%20Vuex%20%EC%BB%B4%ED%8F%AC%EB%84%8C%ED%8A%B8%20%EA%B0%80%EC%9D%B4%EB%93%9C)